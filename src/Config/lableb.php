<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Lableb Config 
    |--------------------------------------------------------------------------
    | You Can Get The Value Of Those Keys Form   https://dashboard.lableb.com/dashboard/login 
    */
    'project_name' =>  env('LABLEB_PROJECT_NAME'),
    'search_token' =>  env('LABLEB_SEARCH_TOKEN'),
    'index_token' =>  env('LABLEB_INDEX_TOKEN'),
    'collection_name' =>  env('LABLEB_COLLECTION_NAME','posts'),

    /*
    |--------------------------------------------------------------------------
    | Disable/Enable Options
    |--------------------------------------------------------------------------
    | By Default options will be enabled 
    | If you want to disable those options put them false
    | 
    */


    /**
     *   Enable Or Disable Autocomplete Dropdown List In Search Box  
     */
    'autocomplete' => env('AUTOCOMPLETE_ENABLE', true),
    /**
     *   Enable Or Disable Autocomplete Dropdown List In Search Box  
     */
    'filters' => env('FILTERS_ENABLE', true),
    /**
     *   Enable Or Disable Paginatoin In Search Result 
     */
    'pagination' => env('PAGINATION_ENABLE', true),
    /**
     *   Show Or Hide Lableb Logo   
     */
    'show_logo' => env('SHOW_LOGO', true),
    /**
     *   Show Or Hide Recommended Documents   
     */
    'recommendation' => env('RECOMMENDATION_ENABLE', true),
];
