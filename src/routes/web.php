<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LablebController;
use Lableb\LablebSDK;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::name('lableb.')->prefix('lableb')->group(function () {
    Route::get('/search', [LablebController::class, 'search'])->name('search');
    Route::get('/autocomplete', [LablebController::class, 'autocomplete'])->name('autocomplete');
    Route::get('/recommend', [LablebController::class, 'recommend'])->name('recommend');
    Route::get('/recommend-feedback', [LablebController::class, 'recommend_feedback'])->name('recommend_feedback');
    Route::get('/search-feedback', [LablebController::class, 'search_feedback'])->name('search_feedback');
    Route::get('/autocomplete-feedback', [LablebController::class, 'autocomplete_feedback'])->name('autocomplete_feedback');
});

Route::name('lableb.views.')->prefix('lableb/views')->group(function(){
    Route::get('/all-examples', [LablebController::class, 'all_examples'])->name('recommend_view');
});