<?php

namespace Lableb\Contracts;

/**
 * Interface  LablebInterface
 *
 * @author  Lableb Team  <support@lableb.com>
 */
interface LablebInterface
{

    /**
     * Indexes documents on Lableb
     *
     * @param collection - collection name on Lableb
     * @param documents - a single document or an array of documents
     *
     * @return Array
     * @throws LablebException
     */
    public function index($collection, $documents);

    /**
     * Searches documents on Lableb
     *
     * @param collection - collection name on Lableb
     * @param query - an associative array of search query parameters
     * @param handler - an optional parameter which refers to the search handler
     *
     * @return Array
     * @throws LablebException
     */
    public function search($collection, $documents);

    /**
     * Searches for autocomplete suggestions
     *
     * @param collection - collection name on Lableb
     * @param options - an associative array of autocomplete query parameters
     * @param handler - name of the search handler [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function autocomplete($collection, $documents);

    /**
     * Fetches related artciles to the one specified in $query
     *
     * @param collection - collection name on Lableb
     * @param query - an associative array describing a document
     * @param hander - suggestion handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function recommend($collection, $documents);

    /**
     * Deletes an indexed document on Lableb
     *
     * @param collection - collection name on Lableb
     * @param id - id of the document to be deleted
     *
     * @return Array
     * @throws LablebException
     */
    public function delete($collection, $id);

    /**
     * Submits a search result click feedback
     *
     * @param collection - collection name on Lableb
     * @param params - an associative array of feedback parameters
     * @param handler - what search handler was used in the search results
     *
     * @return Array
     * @throws LablebException
     */
    public function submitSearchFeedback($collection, $params, $handler = 'default');

    /**
     * Submits autocomplete suggestion click
     *
     * @param collection - collection name on Lableb
     * @param params - an associative array of request body
     * @param handler - used autocomplete handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitAutocompleteFeedback($collection, $params, $handler = 'suggest');

    /**
     * Submits a recommended articles click
     *
     * @param collection - collection name on Lableb
     * @param source - source document
     * @param target - target (clicked) document
     * @param handler - used suggestion handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitRecommendationFeedback($collection, $source, $target, $handler = 'recommend');

}
