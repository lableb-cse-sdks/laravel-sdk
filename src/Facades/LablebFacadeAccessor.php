<?php

namespace Lableb\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class LablebFacadeAccessor
 *
 * @author  Lableb Team  <support@lableb.com>
 */
class LablebFacadeAccessor extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'lableb';
    }
}
