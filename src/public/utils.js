const utils = {

    toggleClassName(element, targetClassName) {

        let classes = Array.from(element.classList);
        let isVisible = classes.find(className => className.includes(targetClassName));
        let newClasses = isVisible
            ?
            classes.filter(className => !className.includes(targetClassName))
            :
            classes.concat(targetClassName);

        element.setAttribute('class', newClasses.join(' '));
    },

    addClassName(element, targetClassName) {
        let classes = Array.from(element.classList);
        element.setAttribute('class', classes.concat(targetClassName).join(' '));
    },

    removeClassName(element, targetClassName) {
        let classes = Array.from(element.classList).filter(className => !className.includes(targetClassName));
        element.setAttribute('class', classes.join(' '));
    },

    deriveFacetsQueryString(facetsState) {

        let queryParams = '';
        const facetsKeys = Object.keys(facetsState);
        for (const facetName of facetsKeys) {
            for (const facetValue of facetsState[facetName]) {
                queryParams += `&${facetName}=${facetValue}`;
            }
        }

        return queryParams;
    }
}




const debounced = (function _debounced() {

    function delay(ms, cb) {

        let token = setTimeout(cb, ms);

        return () => clearTimeout(token);
    }


    let cancelFunction;

    function debounce(cb, timeout) {

        if (cancelFunction) cancelFunction();

        cancelFunction = delay(timeout, cb);
    }

    return debounce;

})();