/**
 * Search result component
 */
const searchResults = {

    /** container */
    _searchResultsContainer: document.getElementsByClassName('lableb-results')[0],

    state: {
        /** save search response here */
        searchResponse: {},

        /** 
         * in case user click on one search result save it here
         * we need it to do the recommendation based on it
         */
        selectedResult: {},
    },

    /**
     * on result click send feedback to Lableb and fetch recommendation accordingly
     */
    onResultClick() {
        lablebClient.client.searchFeedbackFromDocumentData(this.feedback);
        searchResults.state.selectedResult = this;
        Recommendations.fetchRecommendations();
    },


    /** create single DOM search result div from search result object */
    createSingleSearchResult(result) {

        let singleResult = document.createElement('div');
        singleResult.setAttribute('class', 'lableb-result-card');
        singleResult.addEventListener('click', this.onResultClick.bind(result))
        singleResult.innerHTML = `
        <img class="lableb-result-card_img" src="${result?.image}" />

        <p class="lableb-result-card_title">
            ${result?.title}
        </p>

        <div class="lableb-result-card_tags">
            ${result?.tags?.map(tag => `<span class="tag">${tag}</span>`).join(" ") ?? ''}
        </div>
        `;

        return singleResult;
    },


    render() {
        /** empty existing UI */
        this._searchResultsContainer.innerHTML = '';

        /** Run through all results and append to the container */
        if (this.state.searchResponse?.results) {
            this.state.searchResponse?.results
                .forEach(result =>
                    this._searchResultsContainer.appendChild(
                        this.createSingleSearchResult(result)
                    )
                );
        }
    },
}

/** 
 * important to link the component to the BaseComponent though prototype behaviour delegation
 */
Object.setPrototypeOf(searchResults, BaseComponent);
