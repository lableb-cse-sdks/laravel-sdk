/**
 * search filters are derived from `facets` in search response
 * 
 * each facet has a bucket object that contain plenty of values
 * each value will be rendered as a checkbox, which if clicked will cause
 * new search considering this facet value in your search request(will filter all search
 * responses according to the sent facet)
 */
const searchFilters = {

    /** facets container from DOM */
    _facetsFilters: document.getElementsByClassName('lableb-filters')[0],

    state: {
        /** what user has clicked is stored in here */
        selectedFacets: {},

        /** facets returned from search response is saved here */
        facets: {},
    },

    /**
     * selectedFacets has this structure:
     * {
     *      facetName1: [ 'value1', 'value2' ],
     *      facetName2: [ 'value3', 'value4' ],
     *      ...
     * }
     * 
     * this function remove facetValue if it already exist and add it if it doesn't exist
     * @param {string} facetName 
     * @param {string} facetValue 
     */
    toggleFacet(facetName, facetValue) {

        let newFacets = Object.assign({}, this.state.selectedFacets);


        if (newFacets[facetName]) {


            if (newFacets[facetName].includes(facetValue)) {
                newFacets[facetName] = newFacets[facetName].filter(value => value != facetValue)
            } else {
                newFacets[facetName] = newFacets[facetName].concat(facetValue);
            }

        } else {

            newFacets[facetName] = [facetValue];
        }

        /**
         * update current state
         */
        this.setState({ selectedFacets: newFacets });
        /** trigger search */
        searchBox.lablebSearch();
        /** reset pagination */
        searchPagination.setState({ currentPage: 0 });
    },

    render() {

        let facets = this.state.facets;
        /** empty the container first */
        if (this._facetsFilters)
            this._facetsFilters.innerHTML = '';

        /**
         * facets structure is
         * {
         *   facetName1: { buckets: [ { value: '', count: '' }, { value: '', count: '' }, ] },
         *   facetName2: { buckets: [ { value: '', count: '' }, { value: '', count: '' }, ] },
         * }
         * 
         * each facetName will have a div, inside it we render all bucket values as checkboxes(their label
         * is their corresponding buckets value)
         */
        let allFacets = Object.keys(facets)
            .filter((facetName) => {
                /** filter facets not to display empty buckets */
                if (facets[facetName] && facets[facetName].buckets)
                    return facets[facetName].buckets.length > 0
            })
            .map(facetName => {

                /** single div per facet name */
                let singleFacetContainer = document.createElement('div');
                singleFacetContainer.setAttribute('class', 'lableb-filters_facets')

                /** facet name is put in p tag */
                let p = document.createElement('p');
                p.setAttribute('class', 'lableb-filters_facet-name');
                p.innerHTML = facetName;

                singleFacetContainer.appendChild(p);



                /** run through all buckets and convert them to checkboxes */
                let singleFacetCheckboxesContainer = facets[facetName].buckets.map((bucket, index) => {

                    let singleFacetDiv = document.createElement('div');
                    singleFacetDiv.setAttribute('class', 'lableb-filters_single_facet');

                    let facetCheckboxID = `${facetName}-${bucket.value}-${index}`

                    let facetCheckbox = document.createElement('input');
                    facetCheckbox.setAttribute('id', facetCheckboxID);
                    facetCheckbox.setAttribute('type', 'checkbox')
                    facetCheckbox.setAttribute('class', 'lableb-filters_facet_checkbox');
                    if (this.state.selectedFacets[facetName] && this.state.selectedFacets[facetName].includes(bucket.value)) {
                        facetCheckbox.setAttribute('checked', 'true')
                    }
                    facetCheckbox.addEventListener('click', () => {
                        this.toggleFacet(facetName, bucket.value);
                    });

                    /** create facet label */
                    let facetLabel = document.createElement('label');
                    facetLabel.setAttribute('class', 'lableb-filters_facet_label');
                    facetLabel.setAttribute('for', facetCheckboxID);
                    facetLabel.innerHTML = `${bucket.value}`;

                    /** append checkbox inside parent facet div */
                    singleFacetDiv.appendChild(facetCheckbox);
                    singleFacetDiv.appendChild(facetLabel);
                    return singleFacetDiv;
                });

                /** append all checkboxes withing their facet name div container */
                singleFacetCheckboxesContainer.forEach(innerDiv => {
                    singleFacetContainer.appendChild(innerDiv);
                })

                /** single facet with all its buckets rendered as checkboxes */
                return singleFacetContainer;
            });



        /** add header container */
        let header = document.createElement('h6');
        header.setAttribute('class', 'lableb-filters_header');
        header.innerHTML = 'Filters:';

        if (this._facetsFilters) {
            this._facetsFilters.appendChild(header);

            /** insert all facets div(s) into one container */
            allFacets.forEach(singleFacetContainer => {
                this._facetsFilters.appendChild(singleFacetContainer)
            });
        }
    },

}


/** 
 * important to link the component to the BaseComponent though prototype behaviour delegation
 */
Object.setPrototypeOf(searchFilters, BaseComponent);
