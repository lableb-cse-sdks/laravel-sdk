/**
 * one place to use/init lableb client
 */
const lablebClient = {

    client: {},

    init(clientOptions) {
        this.client = window.LablebSDK.init(clientOptions);
    },

}
