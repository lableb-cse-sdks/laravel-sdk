/**
 * pagination number buttons component
 * 
 * we have meta buttons like next/prev buttons, next 10/prev 10 buttons, ...
 * we have special functions to check for the capability of navigation
 *
 * and the state object tell use our current state in UI
 */
const searchPagination = {

    /** pagination container */
    _paginationContainer: document.getElementById('search-pagination'),

    /** selected page class */
    _SELECTED_PAGE_CLASS: 'lableb-pagination_selected-page',


    /** component state */
    state: {
        currentPage: 0,
        pageSize: 10,
        totalItems: 1,
    },


    /** first page button */
    _FIRST_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            this.changePage(0);
        });
        button.setAttribute('class', 'reverse');
        button.innerHTML = `<span>&#8696;</span>`;
        return button;
    },


    /** prev 10 page button */
    _PREVIOUS_10_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            if (this.hasPrevTen())
                this.changePage(this.state.currentPage - 10);
        });
        button.setAttribute('class', 'reverse');
        button.innerHTML = `<span>&#10509;</span>`;
        return button;
    },


    /** prev page button */
    _PREVIOUS_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            if (this.hasPrev())
                this.changePage(this.state.currentPage - 1);
        });
        button.setAttribute('class', 'reverse');
        button.innerHTML = `<span>&#10140;</span>`;
        return button;
    },


    /** last page button */
    _LAST_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            if (this.pagesCount())
                this.changePage(this.pagesCount() - 1);
        });
        button.innerHTML = `<span>&#8696;</span>`;
        return button;
    },


    /** next 10 pages button */
    _NEXT_10_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            if (this.hasNextTen())
                this.changePage(this.state.currentPage + 10);
        });
        button.innerHTML = `<span>&#10509;</span>`;
        return button;
    },


    /** next page button */
    _NEXT_PAGE() {
        let button = document.createElement('button');
        button.addEventListener('click', () => {
            if (this.hasNext())
                this.changePage(this.state.currentPage + 1);
        });
        button.innerHTML = `<span>&#10140;</span>`;
        return button;
    },



    /** 
     * change page function
     * all code should use this central function to change page number
     * which will trigger updating the state and triggering new search request
     */
    async changePage(pageNumber) {
        this.setState({ currentPage: pageNumber });
        await searchBox.lablebSearch();
    },

    /** utilities functions */
    pagesCount() {
        return Math.ceil(this.state.totalItems / this.state.pageSize);
    },
    hasNext() {
        return (this.state.currentPage + 1) < this.pagesCount();
    },
    hasNextTen() {
        return (this.state.currentPage + 11) < this.pagesCount();
    },
    hasPrev() {
        return this.state.currentPage > 0;
    },
    hasPrevTen() {
        return (this.state.currentPage - 10) >= 0;
    },

    /** render function */
    render() {

        const {
            currentPage,
        } = this.state;

        /** 
         * we don't display every possible page
         * we at most display 4 adjacent pages buttons in addition to the 
         * current page number
         * 
         * so we slice pages, considering edge cases like being the last or first pages
         */
        let from = currentPage - 2;
        let to = currentPage + 3;
        if (to > this.pagesCount()) {
            to = this.pagesCount();
            from = to - 5;
        }
        if (from < 0) {
            from = 0;
            to = from + 5;
        }

        const slicedPages = Array(this.pagesCount()).fill(0).map((x, i) => i).slice(from, to);



        /** no need to display any page then */
        if (slicedPages.length < 2) { return; }


        /** convert buttons number to button elements */
        const pagesNumberButtons = slicedPages.map((pageNumber) => {

            let button = document.createElement('button');
            button.addEventListener('click', () => this.changePage(pageNumber));
            button.innerHTML = `${pageNumber + 1}`;

            if (this.state.currentPage == pageNumber) {
                button.setAttribute('class', this._SELECTED_PAGE_CLASS);
            }

            return button;
        });

        /** add meta buttons  */
        let allButtons = [
            this._FIRST_PAGE(),
            this._PREVIOUS_10_PAGE(),
            this._PREVIOUS_PAGE(),
            ...pagesNumberButtons,
            this._NEXT_PAGE(),
            this._NEXT_10_PAGE(),
            this._LAST_PAGE(),
        ]

        if (this._paginationContainer) {
            /** empty existing pages numbers buttons */
            this._paginationContainer.innerHTML = '';

            /** render all buttons */
            allButtons.forEach(button => {
                this._paginationContainer.appendChild(button);
            });
        }

    }
}


/** 
 * important to link the component to the BaseComponent though prototype behaviour delegation
 */
Object.setPrototypeOf(searchPagination, BaseComponent);
