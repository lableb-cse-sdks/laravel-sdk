/**
 * all component has their prototype linked to this object
 * 
 * basic idea is that all component has some state(data container), where state is responsible for what
 * will be displayed in UI, setState a function that update the state, after running we check, if render function
 * exist we call it to update the UI, most UI component will have render function which take the state object
 * and convert it to html and put it in browser
 */
const BaseComponent = {

    state: {},

    setState(newState) {

        this.state = {
            ...this.state,
            ...newState,
        };

        if (this.render && typeof this.render == 'function') {
            this.render()
        }
    },
}