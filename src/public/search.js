/**
 * search and autocomplete component
 */
const searchBox = {

    /** hide/show classes used when hiding/display spinner/autocomplete banner */
    _VISIBLE_AUTOCOMPLETE_CLASS: 'lableb-search-box_autocomplete-is-visible',
    _SPINNER_HIDE_CLASS: 'loader_is-hidden',
    _SEARCH_RECT_CLASS: 'lableb-search-box_search-is-rect',

    /** DOM elements selectors */
    _searchInputContainer: document.getElementsByClassName('lableb-search-box_search')[0],
    _searchInput: document.getElementsByClassName('lableb-search-box_input')[0],
    _searchIcon: document.getElementsByClassName('lableb-search-box_icon')[0],
    _autocompleteContainer: document.getElementsByClassName('lableb-search-box_autocomplete')[0],
    _loaderElement: document.querySelector('.lableb-search-box_icon_loader .loader'),
    _autocompleteResults: document.getElementsByClassName('lableb-search-box_autocomplete_results')[0],


    state: {
        /** current query value(search input value) */
        query: '',
        /** save autocomplete response here */
        autocompleteResponse: {}
    },

    /** update state on input change */
    onSearchInputChange(event) {
        const query = event.target.value;
        this.setState({ query });
    },


    /**
     * send the search request to Lableb
     */
    async lablebSearch() {
        try {

            let query = this.state.query;
            /** no need to continue with empty query */
            if (!query) return;

            this.showSpinner();

            /**
             * skip and limit used for pagination
             * deriveFacetsQueryString will convert selected facets(filters) to be sent with the request
             */
            let searchResponse = await lablebClient.client.search({
                query: query + utils.deriveFacetsQueryString(searchFilters.state.selectedFacets),
                limit: searchPagination.state.pageSize,
                skip: searchPagination.state.currentPage * searchPagination.state.pageSize,
            });

            /** set search/facets/items response in state */
            searchFilters.setState({ facets: searchResponse.facets });
            searchResults.setState({ searchResponse });
            searchPagination.setState({ totalItems: searchResponse.found_documents });

            /** re-render pagination */
            searchPagination.render();

        } catch (error) {
            console.error(error);
        } finally {
            this.hideSpinner();
        }
    },


    /** on Enter key clicked inside input */
    onEnterClick(event) {
        if (event.key == 'Enter') {

            /** make search */
            this.lablebSearch();

            /** reset filters */
            searchFilters.setState({ selectedFacets: {} });
        }
    },

    /** hide autocomplete results container */
    hideAutocompleteList() {
        utils.removeClassName(this._searchInputContainer, this._SEARCH_RECT_CLASS);
        utils.removeClassName(this._autocompleteContainer, this._VISIBLE_AUTOCOMPLETE_CLASS)
    },

    /** show autocomplete results container */
    showAutocompleteList() {
        utils.addClassName(this._searchInputContainer, this._SEARCH_RECT_CLASS);
        utils.addClassName(this._autocompleteContainer, this._VISIBLE_AUTOCOMPLETE_CLASS)
    },

    /** show search spinner */
    showSpinner() {
        utils.removeClassName(this._loaderElement, this._SPINNER_HIDE_CLASS);
    },

    /** hide search spinner */
    hideSpinner() {
        utils.addClassName(this._loaderElement, this._SPINNER_HIDE_CLASS);
    },

    /** on autocomplete result clicked send feedback to Lableb */
    onAutocompleteResultClick() {
        lablebClient.client.autocompleteFeedbackFromDocumentData(this.feedback);
    },



    /** create single autocomplete DOM element from autocomplete object data */
    createSingleAutocompleteResult(text, meta, feedback) {

        if (!text) return;

        let singleAutocompleteRow = document.createElement('div');
        singleAutocompleteRow.setAttribute('class', 'lableb-search-box_autocomplete_results_row');
        singleAutocompleteRow.addEventListener('click', this.onAutocompleteResultClick.bind({ feedback }));

        singleAutocompleteRow.innerHTML = `
            <span class="text">
                ${text}
            </span>
    
            <span class="meta">
                ${meta || ''}
            </span>
        `;

        return singleAutocompleteRow;
    },


    /** fetch autocomplete  */
    fetchSuggestions() {

        /** 
         * debounced function will cancel previous execution in case of cascading functions 
         */
        debounced(async () => {
            try {
                const { query } = this.state;
                if (!query) return;

                this.showSpinner();

                let autocompleteResponse = await lablebClient.client.autocomplete({ query });

                this.hideAutocompleteList();

                /** set autocomplete response */
                this.setState({ autocompleteResponse });

                /** empty autocomplete container and  */
                this._autocompleteResults.innerHTML = '';

                /** re-render autocomplete results */
                autocompleteResponse?.results?.forEach(result =>
                    this._autocompleteResults.append(
                        this.createSingleAutocompleteResult(result.phrase, result.type, result.feedback)
                    )
                );

                this.showAutocompleteList();

            } catch (error) {
                console.error(error);
            } finally {
                this.hideSpinner();
            }
        }, 1500);

    },

    /** on input blur hide autocomplete list */
    onInputBlur() {
        setTimeout(() => {
            this.hideAutocompleteList();
        }, 500);
    },

    /**on input focus display autocomplete response */
    onInputFocus() {
        if (
            this.state.autocompleteResponse &&
            this.state.autocompleteResponse.results &&
            this.state.autocompleteResponse.results.length &&
            this.state.query
        ) {
            this.showAutocompleteList();
        }
    },

    /** on search icon click do new search */
    async onSearchIconClick() {
        
        this.lablebSearch();

        /** reset filters */
        await searchFilters.setState({ selectedFacets: {} });
    },


    /** bind events */
    eventsHandle() {

        this._searchInput.addEventListener('keydown', this.onEnterClick.bind(this));
        this._searchInput.addEventListener('keyup', this.onSearchInputChange.bind(this));
        this._searchInput.addEventListener('keydown', this.fetchSuggestions.bind(this));
       
        this._searchIcon.addEventListener('click', this.onSearchIconClick.bind(this));

        this._searchInput.addEventListener('blur', this.onInputBlur.bind(this));
        this._searchInput.addEventListener('focus', this.onInputFocus.bind(this));
    },


    run() {
        this.eventsHandle();
    },
}


/** 
 * important to link the component to the BaseComponent though prototype behaviour delegation
 */
Object.setPrototypeOf(searchBox, BaseComponent);