/**
 * Lablab Recommendation component(very similar to search results component)
 */
const Recommendations = {

    /** container */
    _recommendationContainer: document.getElementsByClassName('lableb-recommend')[0],

    state: {
        /** save recommend response here */
        recommendResponse: {},
    },

    /** fetch recommendation based on a state found in search results component */
    async fetchRecommendations() {
        if (searchResults.state.selectedResult && searchResults.state.selectedResult.id) {
            let recommendResponse = await lablebClient.client.recommend({ id: searchResults.state.selectedResult.id });
            this.setState({ recommendResponse });
        }
    },

    /** on recommend result click we send feedback to Lableb using the SDK */
    onRecommendClick() {
        lablebClient.client.recommendFeedbackFromDocumentData(this.feedback);
    },


    /** create single DOM element from single recommend response */
    createSingleRecommendResult(result) {

        let singleResult = document.createElement('div');
        singleResult.setAttribute('class', 'lableb-result-card');
        singleResult.addEventListener('click', this.onRecommendClick.bind(result))

        singleResult.innerHTML = `
        <img class="lableb-result-card_img" src="${result.image}" />

        <p class="lableb-result-card_title">
            ${result.title}
        </p>

        `;

        return singleResult;
    },


    /** render recommendations */
    render() {

        if (this._recommendationContainer) {
            /** empty existing UI */
            this._recommendationContainer.innerHTML = '';

            /** convert all data results to DOM elements and append them to the recommend container */
            this.state.recommendResponse.results.forEach(result => {
                this._recommendationContainer.appendChild(
                    this.createSingleRecommendResult(result)
                )
            });
        }
    },
}


/** 
 * important to link the component to the BaseComponent though prototype behaviour delegation
 */
Object.setPrototypeOf(Recommendations, BaseComponent);