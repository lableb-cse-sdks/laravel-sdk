<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lableb Client</title>
    <link rel="stylesheet" href="https://www.unpkg.com/@lableb/sdk-style@0.0.2/dist/core.css" />
    <link rel="stylesheet" href="{{ asset('vendor/lableb-sdk/styles/lableb.css') }}" />
</head>

<body>

    <div class="container">
        @yield('content')
    </div>
    
    <script src="https://unpkg.com/@lableb/javascript-sdk@2.0.4-beta/dist/LablebSDK.min.js"></script>

    <script src="{{ asset('vendor/lableb-sdk/utils.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/base-component.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/client.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/search.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/results.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/filters.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/pagination.js') }}"></script>
    <script src="{{ asset('vendor/lableb-sdk/recommend.js') }}"></script>
    {{-- <script src="{{ asset('vendor/lableb-sdk/app.js') }}"></script> --}}
    <script>
        (function app() {    
        lablebClient.init({
            collectionName: "<?php echo config('lableb.collection_name') ; ?>",
            projectName: "<?php echo config('lableb.project_name'); ?>",
            searchToken: "<?php echo config('lableb.search_token') ;?>",
            searchHandler: 'default',
            autoCompleteHandler: 'suggest',
            recommendHandler: 'recommend'
        });

        searchBox.run();
 
        })();

    </script>
</body>

</html> 