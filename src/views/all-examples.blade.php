@extends('lableb.laravel-sdk.app')
@section('content')

    <!---------------------------->
    <!-- Search input & Autocomplete Results -->
    <div class="lableb-search-box lableb-search-box_rtl">

        <div class="lableb-search-box_search">

            <input class="lableb-search-box_input" placeholder="Search..." spellCheck="false" autoComplete="false" />

            <button class="lableb-search-box_icon">

                <img class="lableb-search-box_icon_img"  src="{{ asset('vendor/lableb-sdk/images/search-yellow.svg') }}" />

                <div class="lableb-search-box_icon_loader">
                    <div class="loader loader_is-hidden"></div>
                </div>


            </button>

        </div>

        @if (config('lableb.autocomplete'))

            <div class="lableb-search-box_autocomplete">

                <div class="lableb-search-box_autocomplete_results">

                </div>

            </div>

        @endif

        <div class="lableb-search-box_identity">
            @if (config('lableb.show_logo'))
                <a href="http://lableb.com/" target="_blank">
                    <img class="logo" src="{{ asset('vendor/lableb-sdk/images/logo.png') }}"  />
                </a>
            @endif
        </div>


    </div>
    <!---------------------------->



    <!---------------------------->
    <!-- Filters + Search Results + Search Pagination -->
    <div style="display: flex;">

        @if (config('lableb.filters'))
            <div class="lableb-filters"> </div>
        @endif

        <div>

            <!-- Display Search Results -->
            <div class="lableb-results"></div>

            @if (config('lableb.pagination'))
                <div id="search-pagination" class="lableb-pagination"></div>
            @endif
        </div>

    </div>
    <!---------------------------->


    @if (config('lableb.recommendation'))
        <!---------------------------->
        <!-- Recommendation -->
        <h2>Recommendations:</h2>
        <p>(click on search result to see recommendations)</p>

        <!-- Display Recommendations -->
        <div class="lableb-results lableb-recommend">
        </div>
        <!---------------------------->
    @endif

@endsection
