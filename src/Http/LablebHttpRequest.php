<?php
namespace Lableb\Http;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Lableb\Exceptions\LablebException;
use Lableb\Helpers\LablebDocumentHelper;
use Lableb\Helpers\LablebResponseHelper;
use Lableb\Helpers\LablebSessionManager;

/**
 * Class LablebHttpRequest
 *
 * @author  Lableb Team  <support@lableb.com>
 */
class LablebHttpRequest
{
    const BASE_URL = 'https://api-bahuth.lableb.com/api/v2/projects/';
    public $projectName;
    public $searchToken;
    public $indexingToken;
    public function __construct($projectName, $searchToken, $indexingToken)
    {
        $this->validateInput([
            'PROJECT_NAME' => $projectName,
            'SEARCH_TOKEN' => $searchToken,
            'INDEXING_TOKEN' => $indexingToken,
        ]);

        $this->projectName = $projectName;
        $this->searchToken = $searchToken;
        $this->indexingToken = $indexingToken;
        
        $this->baseUrl =  self::BASE_URL . $projectName . "/";
        $this->client = new Client(["base_uri" => $this->baseUrl]);
        $this->helper = new LablebDocumentHelper();
        $this->sessionID = (new LablebSessionManager())->getSessionID();

    }

    /**
     * Indexes documents on Lableb
     *
     * @param collection - collection name on Lableb
     * @param documents - a single document or an array of documents
     *
     * @return Array
     * @throws LablebException
     */
    public function indexing($collection, $documents = [])
    {

        // To accept a single document or an array of them
        if (!isset($documents[0])) {
            $documents = [$documents];

        }

        $documents = $this->helper->stringifyDocuments($documents);

        try
        {
            $response = $this->client->request('POST', "collections/$collection/documents", [
                "json" => $documents,
                "query" => ["token" => $this->indexingToken],
            ]);
            return LablebResponseHelper::index_response($response, count($documents));
        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }

    /**
     * Searches documents on Lableb
     *
     * @param collection - collection name on Lableb
     * @param query - an associative array of search query parameters
     * @param handler - an optional parameter which refers to the search handler
     *
     * @return Array
     * @throws LablebException
     */
    public function search($collection, $query, $handler = "default")
    {
        if (empty($query["q"])) {
            throw new \Exception("\"q\" is required in search query");
        }
        try
        {
            $qs = $this->buildQueryString(array_merge($query, ["token" => $this->searchToken, "session_id" => $this->sessionID]));
            $response = $this->client->request("GET", "collections/$collection/search/$handler", [
                "query" => $qs,
            ]);
            $feedbackUrl = $this->baseUrl . "collections/$collection/search/$handler/feedback/hits";

            return LablebResponseHelper::search_response(
                $response,
                $this,
                $feedbackUrl,
                $query["q"]);

        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }

    /**
     * Searches for autocomplete suggestions
     *
     * @param collection - collection name on Lableb
     * @param options - an associative array of autocomplete query parameters
     * @param handler - name of the search handler [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function autocomplete($collection, $query, $handler = "suggest")
    {
        if (empty($query["q"])) {
            throw new \Exception("\"q\" is required in autocomplete query");
        }
        try
        {
            $qs = $this->buildQueryString(array_merge($query, ["token" => $this->searchToken, "session_id" => $this->sessionID]));
            $response = $this->client->request("GET", "collections/$collection/autocomplete/$handler", [
                "query" => $qs,
            ]);
            $feedbackUrl = $this->baseUrl . "collections/$collection/autocomplete/$handler/feedback/hits";

            return LablebResponseHelper::autocomplete_response(
                $response,
                $this,
                $feedbackUrl,
                $query["q"]);

        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }

    public function recommend($collection, $options, $handler = "suggest")
    {
        if (empty($options["id"])) {
            throw new \Exception("\"id\" is required in recommendations search");
        }

        try
        {
            $qs = $this->buildQueryString(array_merge($options, ["token" => $this->searchToken, "session_id" => $this->sessionID]));
            $response = $this->client->request("GET", "collections/$collection/recommend/$handler", [
                "query" => $qs,
            ]);
            $feedbackUrl = $this->baseUrl . "collections/$collection/recommend/$handler/feedback/hits";
                                              
            return LablebResponseHelper::recommend_response(
                $response,
                $this,
                $feedbackUrl,
                $options["id"]
            );

        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }

    public function delete($collection, $id)
    {
        if (empty($id)) {
            throw new \Exception("\"id\" is required in delete documents");
        }
        try
        {
            $response = $this->client->request('DELETE', "collections/$collection/documents/$id", [
                "query" => ["token" => $this->indexingToken],
            ]);

            return LablebResponseHelper::delete_response($response, $id);

        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }
    
    /**
     * Submits a search result click feedback
     *
     * @param collection - collection name on Lableb
     * @param params - an associative array of feedback parameters
     * @param handler - what search handler was used in the search results
     *
     * @return Array
     * @throws LablebException
     */
    public function submitSearchFeedback($collection, $params, $handler)
    {
        try
        {
            $params['session_id'] = $this->sessionID;
            $res = $this->client->request('POST', "collections/$collection/search/$handler/feedback/hits", [
                "query" =>  ["token" => $this->searchToken],
                "json" => $params
                
            ]);

            return ["submitted" => true];
        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }
    /**
     * Submits autocomplete suggestion click
     *
     * @param collection - collection name on Lableb
     * @param params - an associative array of request body
     * @param handler - used autocomplete handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitAutocompleteFeedback($collection, $params, $handler)
    {
        try
        {
            $params['session_id'] = $this->sessionID;
            $res = $this->client->request('POST', "collections/$collection/autocomplete/$handler/feedback/hits", [
                "query" =>  ["token" => $this->searchToken],
                "json" => $params
            ]);

            return ["submitted" => true];
        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
    }

    /**
     * Submits a recommended articles click
     *
     * @param collection - collection name on Lableb
     * @param source - source document
     * @param target - target (clicked) document
     * @param handler - used suggestion handler name [OPTIONAL]
     *
     * @return Array
     * @throws LablebException
     */
    public function submitRecommendationFeedback($collection, $source, $target, $handler)
    {
        $params = [
            "session_id" => $this->sessionID,
            "source_id" => $source['id'],
            "target_id" => $target['id'],
        ];

        if (!empty($source['title'])) {
            $params['source_title'] = $source['title'];
        }

        if (!empty($source['url'])) {
            $params['source_url'] = $source['url'];
        }

        if (!empty($target['title'])) {
            $params['target_title'] = $target['title'];
        }

        if (!empty($target['url'])) {
            $params['target_url'] = $target['url'];
        }

        if (!empty($source['user_id'])) {
            $params['user_id'] = $source['user_id'];
        }

        if (!empty($source['user_ip'])) {
            $params['user_ip'] = $source['user_ip'];
        }

        if (!empty($source['country'])) {
            $params['country'] = $source['country'];
        }

        if (!empty($target['item_order'])) {
            $params['item_order'] = $target['item_order'];
        }

        try
        {
            $res = $this->client->request('POST', "collections/$collection/recommend/$handler/feedback/hits", [
                "query" =>  ["token" => $this->searchToken],
                "json" => $params
            ]);

            return ["submitted" => true];
        } catch (RequestException $e) {
            $this->handleRequestException($e);
        }
 
    }
    /**
     * Handles the Request Exception, it converts the exception into LablebException and then throws it
     *
     * @param e - Request Exception
     *
     * @throws LablebException
     */
    private function handleRequestException($e)
    {
        if ($e->hasResponse()) {
            $body = json_decode($e->getResponse()->getBody(), true);
            throw new LablebException($e->getResponse()->getStatusCode());
        } else {
            throw new LablebException(500);
        }
    }
    /**
     * Passed an array of key-value pairs, it validates that inputs not empty
     *
     * @param [Array] key => value
     */
    private function validateInput($inputs = [])
    {
        foreach ($inputs as $key => $value) {
            if (empty($value)) {
                throw new \Exception("Project Name And Tokens are required");
            }

        }
    }

    /**
     * Passed an array of key-value pairs, it converts it into a compatible querystring with Lableb api
     *
     * @param [Array] $qs
     * @return String
     */
    private function buildQueryString($qs)
    {
        $query = "";
        foreach ($qs as $key => $value) {
            if (empty($value)) {
                continue;
            }

            if (is_array($value)) {
                if (!isset($value[0])) {
                    foreach ($value as $elKey => $elValue) {
                        if (is_array($elValue)) {
                            foreach ($elValue as $el) {
                                $query .= "&" . http_build_query([$key => $elKey . ":\"$el\""]);
                            }
                        } else {
                            $query .= "&" . http_build_query([$key => $elKey . ":\"$elValue\""]);
                        }
                    }
                } else {
                    foreach ($value as $el) {
                        $query .= "&" . http_build_query([$key => $el]);
                    }
                }
            } else {
                $query .= "&" . http_build_query([$key => $value]);
            }
        }

        return substr($query, 1);
    }
}
