<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Lableb\LablebSDK;

class LablebController extends Controller
{
    private $collection = 'posts';
    private $lableb = null;

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct(LablebSDK $lableb)
    {
        $this->lableb = $lableb;
    }

    /**
     * Common Lableb Functionalities.
     *
     */
    public function search(Request $request)
    {
        if ($request->has('q')) {
            try
            {
                $res = $this->lableb->search($this->collection, $request->all());
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }
        } else {
            return response('Query Search Is Required', 400);
        }
    }
    public function autocomplete(Request $request)
    {
        if ($request->has('q')) {
            try
            {
                $res = $this->lableb->autocomplete($this->collection, $request->all());
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }

        } else {
            return response('Query Search Is Required', 400);
        }
    }
    public function recommend(Request $request)
    {
        if ($request->has('id')) {
            try
            {
                $res = $this->lableb->recommend($this->collection, $request->all());
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }

        } else {
            return response('Document ID Is Required', 400);
        }
    }
    /**
     *
     *
     *
     *
     */
    public function search_feedback(Request $request)
    {
        if ($request->has('q') && $request->has('id')) {
            try
            {
                $res = $this->lableb->submitSearchFeedback($this->collection, $request->all());
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }

        } else {
            return response('Document ID Is Required', 400);
        }
    }
    public function autocomplete_feedback(Request $request)
    {
        if ($request->has('q') && $request->has('id')) {
            try
            {
                $res = $this->lableb->submitAutocompleteFeedback($this->collection, $request->all());
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }
            
        } else {
            return response('Document ID Is Required', 400);
        }
    }
    public function recommend_feedback(Request $request)
    {

        if ($request->has('source_id') && $request->has('target_id')) {
            try
            {
                $res =
                $this->lableb->submitRecommendationFeedback($this->collection,
                    ['id' => $request->input('source_id')],
                    ['id' => $request->input('target_id')]);
                return response($res);

            } catch (\Lableb\Exceptions\LablebException $e) {
                echo $e->getStatus() . " - " . $e->getMessage();
            }
            
        } else {
            return response('Document ID Is Required', 400);
        }
    }

    /**
     *
     *
     *
     *
     *
     */
    public function search_view()
    {
        return view('lableb.laravel-sdk.search', ['name' => 'Search View']);
    }
    public function search_result_view()
    {
        return view('lableb.laravel-sdk.search-result', ['name' => 'Search Resul View']);
    }
    public function recommend_result_view()
    {
        return view('lableb.laravel-sdk.recommend-result', ['name' => 'Recommend Result View']);
    }
    public function all_examples()
    {
        return view('lableb.laravel-sdk.all-examples', ['name' => 'All Example View']);
    }
}
