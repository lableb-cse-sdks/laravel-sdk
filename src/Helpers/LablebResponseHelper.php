<?php
namespace Lableb\Helpers;

use Lableb\Helpers\LablebDocumentHelper;

class LablebResponseHelper
{

    /**
     * LablebResponseHelper constructor.
     *
     */
    public function __construct()
    {
    }

    public static function index_response($response, $documents_count)
    {
        $body = json_decode($response->getBody(), true)["response"];

        return [
            "indexed" => true,
            "message" => $documents_count . " has been indexed",
        ];
    }
    public static function search_response($response, $config, $feedbackUrl, $query)
    {
        $body = json_decode($response->getBody(), true)["response"];
        $facets = [];
        $facetsCount = 0;

        if (!empty($body["facets"]) && is_array($body["facets"])) {
            $facetsCount = $body["facets"]["count"];
            foreach ($body["facets"] as $name => $values) {
                if (is_array($values)) {
                    $facets[$name] = $values["buckets"];
                }
            }
        }
        return [
            "totalDocuments" => $body["found_documents"],
            "results" => (new LablebDocumentHelper())->addSearchFeedbackUrls(
                $feedbackUrl,
                $body["results"],
                [
                    'token' => $config->searchToken,
                    'query' => $query,
                    "session_id" => $config->sessionID,
                ]
            ),
            "totalFacets" => $facetsCount,
            "facets" => $facets,
        ];
        return $resporesponsense;
    }

    public static function autocomplete_response($response, $config, $feedbackUrl, $query)
    {
        $body = json_decode($response->getBody(), true)["response"];

        $facets = [];
        $facetsCount = 0;

        if (!empty($body["facets"]) && is_array($body["facets"])) {
            $facetsCount = $body["facets"]["count"];
            foreach ($body["facets"] as $name => $values) {
                if (is_array($values)) {
                    $facets[$name] = $values["buckets"];
                }
            }
        }
        return [
            "totalSuggestions" => $body["found_documents"],
            "suggestions" => $config->helper->addSearchFeedbackUrls(
                $feedbackUrl,
                $body["results"],
                [
                    'token' => $config->searchToken,
                    'query' => $query,
                    "session_id" => $config->sessionID,
                ]
            ),
            "totalFacets" => $facetsCount,
            "facets" => $facets,
        ];
    }
    public static function recommend_response($response, $config, $feedbackUrl, $query)
    {
        $body = json_decode($response->getBody(), true)["response"];
        $facets = [];
        $facetsCount = 0;
        if (!empty($body["facets"]) && is_array($body["facets"])) {
            $facetsCount = $body["facets"]["count"];
            foreach ($body["facets"] as $name => $values) {
                if (is_array($values)) {
                    $facets[$name] = $values["buckets"];
                }
            }
        }
        return [ 
            "totalDocuments" => $body["found_documents"],
            "results" =>  $config->helper->addRecommendFeedbackUrls(
                $feedbackUrl,
                $body["results"],
                [
                    'token' => $config->searchToken,
                    'source_id' => $query,
                    "session_id" => $config->sessionID,
                ]
            ),
            "totalFacets" => $facetsCount,
            "facets" => $facets,
        ];
    }
    public static function delete_response($response, $id)
    {
        $body = json_decode($response->getBody(), true)["response"];

        return [
            "deleted" => true,
            "message" => "Document with ID of $id has been deleted.",
        ];

    }

}
