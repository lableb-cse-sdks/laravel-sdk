<?php
namespace Lableb\Tests;

require 'TestCase.php';

use Lableb\Tests\TestCase;

/**
 * Class LablebTest
 *
 * @category Test
 * @package  Lableb\Tests
 * @author  Lableb Team  <support@lableb.com>
 */
class LablebTest extends TestCase
{
    /**
     * @covers  LablebSDK::index
     */
    public function testIndexing()
    {
        $response = parent::$lableb->index(parent::$collection, parent::$indexing_documents);

        $this->assertEquals($response['indexed'], true);

    }

    /**
     * @covers  LablebSDK::search
     */
    public function testSearch()
    {
        $options = [
            "q" => 'title',
            "category" => "cat2",
            "tags" => ["tag1", "tag2"],
        ];

        $response = parent::$lableb->search(parent::$collection, $options);

        $this->assertTrue(is_array($response['results']));
    }

    /**
     * @covers  LablebSDK::autocomplete
     */
    public function testAutocomplete()
    {
        $options = [
            "q" => 'this',
        ];

        $response = parent::$lableb->autocomplete(parent::$collection, $options);
        $this->assertTrue(is_array($response['suggestions']));
    }

    /**
     * @covers  LablebSDK::recommend
     */
    public function testRecommendations()
    {
        $options = [
            'id' => '1',
        ];

        $response = parent::$lableb->recommend(parent::$collection, $options);
        $this->assertTrue(is_array($response['results']));
    }

    /**
     * @covers  LablebSDK::delete
     */
    public function testSearchFeedback()
    {
        $response = parent::$lableb->submitSearchFeedback(parent::$collection, parent::$feedback_query);
        $this->assertEquals($response['submitted'], 1);
    }
    /**
     * @covers  LablebSDK::delete
     */
    public function testAutocompleteFeedback()
    {
        $response = parent::$lableb->submitAutocompleteFeedback(parent::$collection, parent::$feedback_query);
        $this->assertEquals($response['submitted'], 1);
    }
    /**
     * @covers  LablebSDK::delete
     */
    public function testRecommendationFeedback()
    {
        $response = parent::$lableb->submitRecommendationFeedback(parent::$collection, parent::$source_feedback_query, parent::$target_feedback_query);
        $this->assertEquals($response['submitted'], 1);
    }
    /**
     * @covers  LablebSDK::delete
     */
    public function testDelete()
    {
        $response = parent::$lableb->delete(parent::$collection, rand(1, 6));
        $this->assertTrue($response['deleted']);
    }

}
