<?php

namespace Lableb\Tests;

require_once 'env.php';

use Lableb\LablebSDK;
// use PHPUnit_Framework_TestCase as PHPUnit;
use PHPUnit\Framework\TestCase as PHPUnit;

/**
 * Class TestCase
 *
 * @author  Lableb Team  <support@lableb.com>
 */
class TestCase extends PHPUnit
{
    protected static $lableb;
    protected static $collection ;
    protected static $feedback_query ;
    protected static $source_feedback_query ;
    protected static $target_feedback_query ;
    protected static $indexing_documents ;

    public function __construct()
    {
        parent::__construct();
        self::$collection = 'posts';
        self::$lableb = new LablebSDK(
            getenv('PROJECT_NAME'),
            getenv('SEARCH_TOKEN'),
            getenv('INDEXING_TOKEN')
        );
        self::$indexing_documents = [[
                "id" => 1,
                "title" => "title 1",
                "content" => "this is article content",
                "category" => ["cat1", "cat2"],
                "tags" => ["tag1", "tag2"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team"],
                "date" => new \DateTime(),
            ], [
                "id" => 2,
                "title" => "title 2",
                "content" => "this is article content",
                "category" => ["cat1", "cat2"],
                "tags" => ["tag1", "tag2"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team"],
                "date" => new \DateTime(),
            ], [
                "id" => 3,
                "title" => "title 3",
                "content" => "this is article content",
                "category" => ["cat3"],
                "tags" => ["tag3", "tag4"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team"],
                "date" => new \DateTime(),
            ], [
                "id" => 4,
                "title" => "title 4",
                "content" => "this is article content",
                "category" => ["cat4"],
                "tags" => ["tag4"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team"],
                "date" => new \DateTime(),
            ], [
                "id" => 5,
                "title" => "title 5",
                "content" => "this is article content",
                "category" => ["cat5"],
                "tags" => ["tag5"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team"],
                "date" => new \DateTime(),
            ], [
                "id" => 6,
                "title" => "title 6",
                "content" => "this is article content",
                "category" => ["cat6"],
                "tags" => ["tag6"],
                "url" => "https://solutions.lableb.com/en/doc/php-sdk/index-documents",
                "authors" => ["Lableb Team -Bashar Modallal"],
                "date" => new \DateTime(),
            ],
        ];

        self::$feedback_query = [
            'query' => 'How to make pizza',
            'item_id' => rand(1,6),
            'url' => 'https://funfunfood.com/2018/12/06/pizza',
        ];

        self::$source_feedback_query = [
            "query"=> "سامسونج",
            "id"=> "1",
            "user_id"=> "1",
            "user_ip"=> "167.114.64.183",
            "url"=> "https://RLOCME0425.expandcart.com/product/product&product_id=129",
            "title"=> "LG 55 inch LCD HDTV - TV with 4K in",
            "country"=>'damas'
        ];
        
        self::$target_feedback_query = [
            'id' => 2,
            'item_order'=>"2",
            "url"=> "https://RLOCME0425.expandcart.com/product/product&product_id=130",
            "title"=> "LG 601 inch LCD HDTV - TV with 2K in"
        ];
    }

    public function randString($n = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $n; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }

}
