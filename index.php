<?php

$file =  __DIR__. '/vendor/autoload.php';

if (!file_exists($file)) { 
    exit;
}

require_once $file;

// TO Run Test ; In Your Base Directory Execute That
//./vendor/bin/phpunit --verbose --testdox  --stderr

/******************************************************* */

// php artisan vendor:publish --provider="Lableb\LablebServiceProvider"
