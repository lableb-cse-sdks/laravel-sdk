# Lableb Cloud Search Laravel Package

[![Software License][ico-license]](LICENSE.md)


 Easily integrate with Lableb to add powerful search capabilities in-website/in-app :
 - search (instant results & highly configurable)
 - autocomplete (super fast & understand what you want)
 - recommend (direct links between your data can be fetching using smart recommendation)
 - index new data(documents) (indexing data make it available in search results)
 - update existing data (documents get updated all the times...no problem, you can update it easily)
 - delete documents(easily remove from search results)

## To get your tokens and other config options
 - login to [Lableb Dashboard](https://dashboard.lableb.com/dashboard/login)
 - open API Keys tab
 - generate new tokens(or use the existing ones)
 - copy project name as it is(in small letters)


## Installation

Install using composer:

```
$ composer require lableb/laravel-sdk
```

Then To Publish lableb.php File To Your Config Directory:

```
$ php artisan vendor:publish --provider="Lableb\ServiceProviders\LablebServiceProvider"
```
Add The Following Keys To Your Environment Variables File `.env` file 

( You Can Get The Values From  https://dashboard.lableb.com/dashboard/login ) :


```
LABLEB_PROJECT_NAME=your_project_name
LABLEB_SEARCH_TOKEN=xxxxx-search-token-xxxxxx
LABLEB_INDEX_TOKEN=xxxxx-index-token-xxxxxx
```


You May Probably Need To Clear Cache After That 
Using The Following Commands :

```
$ php artisan config:clear
```

## Usage

you can initialize the Package:

```php
use Lableb\LablebSDK;

$lableb = new LablebSDK(); 
```

You Can Also Pass Parameters Like This

```php
use Lableb\LablebSDK;

$lableb = new LablebSDK(
  "project name",
  "search token",
  "indexing token"
);
```

### Notice
>it is preferred to keep your tokens out of your source code
you can save them in an environment variables file then add it to `.gitignore` for example .

___

>All **LablebSDK** methods throws **LablebException** exception in case of an exception happens. Use the sdk with try-catch

## Index Documents

`$lableb->index( $collection, $documents )`

- **\$collection**: what collection you want to index documents on, e.x `posts`.
- **\$documents**: a document or an array of documents to be indexed.

**Example**:

```php
$collection = 'posts';
$documents = [[
  "id" => 1,
  "title" => "this is a title 1",
  "content" => "this is article content",
  "category" => ["cat1", "cat2"],
  "tags" => ["tag1", "tag2"],
  "url" => "https://solutions.lableb.com/en/doc/laravel-sdk/index-documents",
  "authors" => ["Lableb Team"],
  "date" => new \DateTime()
]];

try
{
  $response = $lableb->index( $collection, $documents );
}
catch ( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getStatus()." - ".$e->getMessage();
}

```

**Example response**:

```php
[
  "indexed" => true,
  "message" => "1 documents has been indexed"
]
```

## Search Documents

`$lableb->search( $collection, $queryOptions, $handler )`

- **\$collection**: collection name you want to search in.
- **\$queryOptions**: an associative array of search parameters.
- **\$handler**: an optional parameter that has the value of `default` by default, specify it if you want to use a custom search handler.

**Search Parameters (\$queryOptions)**:

| Option       | Description                         | Required | Example                                                             |
| ------------ | ----------------------------------- | -------- | ------------------------------------------------------------------- |
| q            | search term                         | yes      | 'how to make pizza'                                                 |
| filter       | search filters                      | no       | [ 'tags' => 'delicious' ]                                           |
| limit        | how many documents to fetch         | no       | 10                                                                  |
| skip         | how many documents to skip (offset) | no       | 50                                                                  |
| sort         | order of documents                  | no       | 'date asc' or 'date desc', in general it's field_name + order       |
| user_id      | a unique user id                    | no       | 1254                                                                |
| user_ip      | user IP address                     | no       | '55.22.11.6'                                                        |
| user_country | user country code                   | no       | 'CA'                                                                |

You can specify any additional query string parameters if you need to, for example `'lang' => 'ar'` or `'tag' => ['tag1', 'tag2']`.

**Example**:

```php
try
{
  $response = $lableb->search( 'posts', [
    'q' => 'how to make pizza',
    'filter' => [
      'category' => 'Food',
      'tags' => [ 'delicious', 'pizza' ]
    ]
  ] );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getStatus()." - ".$e->getMessage();
}
```

**Example Response**:

```php
[
  'totalDocuments' => 50,
  'results' => [
    [
      'id' => 1,
      'title' => 'How to make italian pizza',
      'content' => 'The italian pizza, margherita ...',
      'date' => '2018-12-06T12:16:00.000+0000',
      'url' => 'https://funfunfood.com/2018/12/06/pizza',
      'image' => 'https://funfunfood.com/static/pizza.png',
      'feedbackUrl' => 'https://api-bahuth.lableb.com/api/v2/....'
    ],
  ],
  'totalFacets' => 50,
  'facets' => [
    'categories' => [
      [
        'value' => 'Food',
        'count' => 8
      ]
    ],
    'tags' => [],
    'authors' => [],
    'year' => []
  ]
]
```
### Notice

Each result will have an additional field called **feedbackUrl** which you can perform a GET request on to submit search feedback



## Autocomplete

`$lableb->autocomplete( $collection, $queryOptions, [$handler] )`

- **\$collection**: collection name you want to search for suggestions in.
- **\$queryOptions**: an associatve array with search parameters.
- **\$handler**: an optional parameter that has the value of `suggest` by default, specify it if you want to use a custom autocomplete handler.

**Autocomplete Options (\$queryOptions)**:

| Option       | Description                  | Required            | Example      |
| ------------ | ---------------------------  | ------------------  | --------     |
| q            | search term                  | yes                 | 'how to ma'  |
| limit        | how many documents to fetch  | no                  | 10           |
| user_id      | a unique user id             | no                  | 1254         |
| user_ip      | user IP address              | no                  | '55.22.11.6' |
| user_country | user country code            | no                  | 'CA'         |

You can specify any additional query string parameters if you need to.

**Example**:

```php
try
{
  $response = $lableb->autocomplete( 'posts', [
    'q' => 'how to ma'
  ] );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getMessage();
}
```

**Example response**:

There are two types of suggestions:

1. **Navigational** suggestion which refers to an article
2. **Filter** suggestion which has search filters that can be used with search

```php
[
  'totalSuggestions' => 33,
  'suggestions' => [
    [
      'suggestion_type' => 'navigational',
      'id' => 1,
      'phrase' => 'How to make italian pizza',
      'date' => '2018-12-06T12:16:00.000+0000',
      'url' => 'https://funfunfood.com/2018/12/06/pizza',
      'feedbackUrl' => 'https://api-bahuth.lableb.com/api/v2/....'
    ],
    [
      'suggestion_type' => 'filter',
      'phrase' => 'Posts about pizza',
      'filters' => [
        'meta' => ['Pizza', 'Food']
      ]
    ]
  ]
  'totalFacets' => 0
  'facets' => Array
        (
        )
]
```

### Notice

Each result will have an additional field called **feedbackUrl** which you can perform a GET request on to submit autocomplete feedback.


## Recommendations (Related Articles)

To fetch other posts that are related to some post

`$lableb->recommend( $collection, $options, [$handler] )`

- **\$collection**: collection name you want to search for recommendations in.
- **\$options**: an associative describing the source document.
- **\$handler**: an optional parameter that has the value of `recommend` by default, specify it if you want to use a custom search handler.

**Source document parameters (\$options)**:

| Option       | Description                  | Required | Example                                   |
| ------------ | ---------------------------- | -------- | ----------------------------------------- |
| id           | ID of the source document    | yes      | 1                                         |
| title        | title of the source document | no       | 'How to make italian pizza'               |
| url          | url of the source document   | no       | 'https://funfunfood.com/2018/12/06/pizza' |
| limit        | how many documents to fetch  | no       | 5                                         |
| user_id      | a unique user id             | no       | 1254                                      |
| user_ip      | user IP address              | no       | '55.22.11.6'                              |
| user_country | user country code            | no       | 'CA'                                      |

```php
try
{
  $response = $lableb->recommend( 'posts', [
    'id' => 1
  ] );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getMessage();
}
```

**Example response**:

```php
[
  'totalDocuments' => 50,
  'results' => [
    [
      'id' => 1,
      'title' => 'How to make italian pizza',
      'content' => 'The italian pizza, margherita ...',
      'date' => '2018-12-06T12:16:00.000+0000',
      'url' => 'https://funfunfood.com/2018/12/06/pizza',
      'image' => 'https://funfunfood.com/static/pizza.png',
      'feedbackUrl' => 'https://api-bahuth.lableb.com/api/v2/....'
    ],
  ],
  'totalFacets' => 0
  'facets' => Array
        (
        )
]
```

### Notice

Each result will have an additional field called **feedbackUrl** which you can perform a GET request on to submit recommend feedback.



## Submit Feedback

There are three types of feedback:

- **Search feedback**: this feedback is submitted when a user searches then hits a search result.
- **Autocomplete feedback**: this feedback is submitted when a user hits an autocomplete suggestion.
- **Recommendation feedback**: this feedback is submitted when a user hits a recommended article.

### Submit Search Feedback

`$lableb->submitSearchFeedback( $collection, $options, [$handler] )`

- **\$collection**: collection name that search has been made in.
- **\$options**: an associative describing the hit document.
- **\$handler**: an optional parameters which defaults to `default` and it is handler name used in search.

**Search Feedback Options (\$options)**:

| Option       | Description                                                        | Required | Example                                   |
| ------------ | ------------------------------------------------------------------ | -------- | ----------------------------------------- |
| query        | what was the query user searched for                               | yes      | 'how to make pizza'                       |
| item_id      | the ID of the hit result                                           | yes      | 1                                         |
| item_order   | the order of the hit in the results **starting from 1 not from 0** | yes      | 1                                         |
| url          | url of the source document                                         | yes      | 'https://funfunfood.com/2018/12/06/pizza' |
| user_id      | a unique user id                                                   | no       | 1254                                      |
| user_ip      | user IP address                                                    | no       | '55.22.11.6'                              |
| user_country | user country code                                                  | no       | 'CA'                                      |

**Example**:

```php
try
{
  $lableb->submitSearchFeedback( 'posts', [
    'query' => 'How to make pizza',
    'item_id' => 1,
    'item_order' => 1,
    'url' => 'https://funfunfood.com/2018/12/06/pizza'
  ] );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getMessage();
}
```

**Example response**:

```php
[
  'submitted' => true
]
```

### Submit Autocomplete Feedback

`$lableb->submitAutocompleteFeedback( $collection, $options, [$handler] )`

Exactly same as Search feedback submission but **item_id** and **url** are not required for navigational suggestions.

### Submit Recommendation Feedback

`$lableb->submitRecommendationFeedback( $collection, $source, $target, [$handler] )`

- **\$collection**: collection name that search for recommendations has been made in.
- **\$source**: the source document.
- **\$target**: the hit document.
- **\$handler**: an optional parameters which defaults to `default` and it is handler name used in search.

**Example**:

```php
try
{
  $source = [
    'id' => 1, // REQUIRED
    'title' => 'How to make italian pizza',
    'url' => 'https://funfunfood.com/2018/12/06/pizza',

    // OPTIONAL fields and only specified in source
    'user_id' => 2582,
    'user_ip' => '25.25.12.4',
    'user_country' => 'CA'
  ];

  $target = [
    'id' => 2, // REQUIRED
    'title' => 'How to make margherita',
    'url' => 'https://funfunfood.com/2018/12/06/margherita',

    // Only in target
    'item_order' => 1
  ];

  $response = $lableb->submitRecommendationFeedback( 'posts', $source, $target );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getMessage();
}
```

**Example response**:

```php
[
  'submitted' => true
]
```


## Delete Documents

`$lableb->delete( $collection, $id )`

- **\$collection**: collection name you want to delete the document from.
- **\$id**: ID of the document to be deleted.

**Example**:

```php
try
{
  $response = $lableb->delete( 'posts', 1 );
}
catch( \Lableb\Exceptions\LablebException $e )
{
  echo $e->getMessage();
}
```

**Example response**:

```php
[
  'deleted' => true,
  'message' => 'Document with ID of 1 has been deleted'
]
```




### SDK offers:
 - `indexing` documents function (indexing data make it available in search results)
 - `delete` document function (easily remove from search results)
 - `search` function that enable you to send queries to Lableb API
 - `autocomplete` function that help your user predicting what they are searching for
 - `recommend` function help you get similar document given an existing document
 - `feedback` function for `search` & `autocomplete` & `recommend` which is highly important to you to track your users search choices & recommendation in Lableb dashboard

## Security

If you discover any security related issues, please email support@lableb.com instead of using the issue tracker.

## Credits
- [Lableb][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.


[ico-version]: https://img.shields.io/packagist/v/:vendor/:package_name.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/:vendor/:package_name.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/lableb/laravel-sdk
[link-downloads]: https://packagist.org/packages/lableb/laravel-sdk
[link-author]: https://gitlab.com/lableb-labs
